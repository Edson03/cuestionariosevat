import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Cardiovascular1PageRoutingModule } from './cardiovascular1-routing.module';

import { Cardiovascular1Page } from './cardiovascular1.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Cardiovascular1PageRoutingModule,
    ComponentsModule
  ],
  declarations: [Cardiovascular1Page]
})
export class Cardiovascular1PageModule {}
