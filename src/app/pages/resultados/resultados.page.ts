import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.page.html',
  styleUrls: ['./resultados.page.scss'],
})
export class ResultadosPage implements OnInit {

  constructor(private router: Router) {
    this.Siguiente;
   }
  data = [
    {
      label: 'Neurológico',
      type: 'number',
      value: '3'
    },
    {
      label: 'Cardiovascular',
      type: 'number',
      value: '3'
    },
    {
      label: 'Respiratorio',
      type: 'number',
      value: '3'
    },
    {
      label: 'Preocupación',
      type: 'number',
      value: '2'
    },
    {
      label: 'Total',
      type: 'number',
      value: '12'
    }
  ]
  ngOnInit() {
  }
  Siguiente(){
    //Limpiamos las variables de Sesion Utilizadas
    sessionStorage.clear();
    this.router.navigate(['/inicio']);
  }

}
