import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EstadoNeurologicoPage } from './estado-neurologico.page';

const routes: Routes = [
  {
    path: '',
    component: EstadoNeurologicoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EstadoNeurologicoPageRoutingModule {}
