import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-estado-neurologico',
  templateUrl: './estado-neurologico.page.html',
  styleUrls: ['./estado-neurologico.page.scss'],
})
export class EstadoNeurologicoPage implements OnInit {

  constructor(private router: Router) { }
  item1(x: number, y: string) {
    this.router.navigate(['/cardiovascular']);
  }

  ngOnInit() {
  }

}
