import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EstadoNeurologicoPageRoutingModule } from './estado-neurologico-routing.module';

import { EstadoNeurologicoPage } from './estado-neurologico.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EstadoNeurologicoPageRoutingModule
  ],
  declarations: [EstadoNeurologicoPage]
})
export class EstadoNeurologicoPageModule {}
