import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cardiovascular3',
  templateUrl: './cardiovascular3.page.html',
  styleUrls: ['./cardiovascular3.page.scss'],
})
export class Cardiovascular3Page implements OnInit {

  constructor() { }
  data =[
    {
      label: ' ... está marmóreo? ',
      selected: false,
      value: 3,
      name: 'El paciente está marmóreo'
    },
    {
      label: '... tiene bradicadrdia sintomática?',
      selected: false,
      value: 3,
      name: 'El paciente tiene bradicadrdia sintomática'
    },
    {
      label: '... tiene arritmias?',
      selected: false,
      value: 3,
      name: 'El paciente tiene arritmias'
    }
]
  ngOnInit() {
  }

}
