import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Cardiovascular3Page } from './cardiovascular3.page';

const routes: Routes = [
  {
    path: '',
    component: Cardiovascular3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Cardiovascular3PageRoutingModule {}
