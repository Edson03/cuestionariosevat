import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Cardiovascular3PageRoutingModule } from './cardiovascular3-routing.module';

import { Cardiovascular3Page } from './cardiovascular3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Cardiovascular3PageRoutingModule
  ],
  declarations: [Cardiovascular3Page]
})
export class Cardiovascular3PageModule {}
