import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { IonicModule } from '@ionic/angular';

import { Respiratorio4PageRoutingModule } from './respiratorio4-routing.module';

import { Respiratorio4Page } from './respiratorio4.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Respiratorio4PageRoutingModule,
    ComponentsModule
  ],
  declarations: [Respiratorio4Page]
})
export class Respiratorio4PageModule {}
