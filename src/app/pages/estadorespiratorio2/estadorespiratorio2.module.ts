import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Estadorespiratorio2PageRoutingModule } from './estadorespiratorio2-routing.module';

import { Estadorespiratorio2Page } from './estadorespiratorio2.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Estadorespiratorio2PageRoutingModule,
    ComponentsModule
  ],
  declarations: [Estadorespiratorio2Page]
})
export class Estadorespiratorio2PageModule {}
