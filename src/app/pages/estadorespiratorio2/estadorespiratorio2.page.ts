import { Component, OnInit } from '@angular/core';
import { Popinfo2Component } from '../../components/popinfo2/popinfo2.component';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-estadorespiratorio2',
  templateUrl: './estadorespiratorio2.page.html',
  styleUrls: ['./estadorespiratorio2.page.scss'],
})
export class Estadorespiratorio2Page implements OnInit {

  constructor(private popoverCtrl: PopoverController) { }

  ngOnInit() {
  }

  async mostrarPop2( evento2 ) {

    const popover = await this.popoverCtrl.create({
      component: Popinfo2Component,
      event: evento2,
      mode: 'ios',
    });

    await popover.present();
  }

}
