import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Neurologico1Page } from './neurologico1.page';

describe('Neurologico1Page', () => {
  let component: Neurologico1Page;
  let fixture: ComponentFixture<Neurologico1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Neurologico1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Neurologico1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
