import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-neurologico1',
  templateUrl: './neurologico1.page.html',
  styleUrls: ['./neurologico1.page.scss'],
})
export class Neurologico1Page implements OnInit {

  constructor(private router: Router) { }
  adelante(){//esta no importa que valor tenga solo va para adeltante
    this.router.navigate(['/pagina2'])
  }
  regreso(){
    this.router.navigate(['/estado-neurologico'])
  }
// en los metodos se envia la pagina,estado,checkbox y el punteo
  ir_salida1(){//esta es de la primera opcion
    this.router.navigate(['/cardiovascular']);
  }
  ir_salida2(){
    this.router.navigate(['/cardiovascular'])
  }
  ir_salida3(){
    this.router.navigate(['/cardiovascular'])
  }
  ngOnInit() {
  }

}
