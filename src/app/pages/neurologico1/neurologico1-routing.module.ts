import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Neurologico1Page } from './neurologico1.page';

const routes: Routes = [
  {
    path: '',
    component: Neurologico1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Neurologico1PageRoutingModule {}
