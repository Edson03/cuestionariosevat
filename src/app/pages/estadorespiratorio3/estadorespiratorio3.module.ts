import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Estadorespiratorio3PageRoutingModule } from './estadorespiratorio3-routing.module';

import { Estadorespiratorio3Page } from './estadorespiratorio3.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Estadorespiratorio3PageRoutingModule,
    ComponentsModule
  ],
  declarations: [Estadorespiratorio3Page]
})
export class Estadorespiratorio3PageModule {}
