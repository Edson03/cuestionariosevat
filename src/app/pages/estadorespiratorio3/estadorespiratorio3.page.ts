import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { PopinfoComponent } from '../../components/popinfo/popinfo.component';

@Component({
  selector: 'app-estadorespiratorio3',
  templateUrl: './estadorespiratorio3.page.html',
  styleUrls: ['./estadorespiratorio3.page.scss'],
})
export class Estadorespiratorio3Page implements OnInit {

  constructor( private popoverCtrl: PopoverController ) { }

  ngOnInit() {
  }

  async mostrarPop(evento ) {

    const popover = await this.popoverCtrl.create({
      component: PopinfoComponent,
      event: evento,
      mode: 'ios',
    });

    await popover.present();
  }

}
