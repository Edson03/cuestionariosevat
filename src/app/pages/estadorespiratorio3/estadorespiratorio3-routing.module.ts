import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Estadorespiratorio3Page } from './estadorespiratorio3.page';
import { PopinfoComponent } from '../../components/popinfo/popinfo.component';
import { ComponentsModule } from '../../components/components.module';

const routes: Routes = [
  {
    path: '',
    component: Estadorespiratorio3Page
  }
];

@NgModule({
  entryComponents: [
    PopinfoComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    ComponentsModule
  ],
  exports: [RouterModule],
})
export class Estadorespiratorio3PageRoutingModule {}
