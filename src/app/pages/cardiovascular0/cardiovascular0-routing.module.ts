import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Cardiovascular0Page } from './cardiovascular0.page';

const routes: Routes = [
  {
    path: '',
    component: Cardiovascular0Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Cardiovascular0PageRoutingModule {}
