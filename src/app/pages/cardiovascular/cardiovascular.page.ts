import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-cardiovascular',
  templateUrl: './cardiovascular.page.html',
  styleUrls: ['./cardiovascular.page.scss'],
})
export class CardiovascularPage implements OnInit {
  cardiovascular={
    fcardiaca:'',
    llenado:'',
    estado:'',
    valor: 0,
  }
   estadocapilar:string="";
   valorcapilar:number=0;
   estadofrecuencia:string="";
   valorfrecuencia:number=0;
   inicial:number=0; final:number=0;
   aux:number=0;
   estado:string="";
                        //Edad   Normal,  Leve,    Moderada Severa
                        //       min-max, min-max, min-max, max
frecuencia:number[][] =[ [ 1,2,  119,164, 165,171, 172,186, 187],
                         [ 3,5,  114,159, 160,167, 168,182, 183],
                         [ 6,8,  110,156, 157,163, 164,178, 179],
                         [ 9,11, 107,153, 154,160, 161,176, 177],
                         [12,17, 103,149, 150,157, 158,173, 174],
                         [18,23,  98,146, 147,154, 155,170, 171],
                        //Edad   Normal,  Leve,    Moderada Severa
                        //       min-max, min-max, min-max, max
                         [ 1,1,  103,149, 150,157, 158,173, 174],
                         [ 2,2,   93,142, 143,150, 151,167, 168],
                         [ 3,3,   88,138, 139,146, 147,164, 165],
                         [ 4,5,   83,134, 135,142, 143,161, 162],
                         [ 6,7,   77,128, 129,137, 138,155, 156],
                         [ 8,11,  72,120, 121,129, 130,147, 148],
                         [12,14,  66,112, 113,121, 122,138, 139],
                         [15,18,  62,107, 108,115, 116,132, 133],
                         [18,150, 51,100, 101,110, 111,129, 130],
]
  
  constructor(public alertCtrl: AlertController,private router: Router) { 
    this.Cardio();
  }

  ngOnInit() {
    this.cardiovascular;
  }

  Cardio(){
    let paciente = JSON.parse(sessionStorage.getItem("Paciente"));
     if(paciente.tipoedad=="años"){
      this.inicial=6;
      this.final=14;
    } else if(paciente.tipoedad=="meses"){
      this.inicial=0; 
      this.final=5;
    }
    for(var f:number = this.inicial; f<=this.final; f++){
      if(parseInt(paciente.edad)>=this.frecuencia[f][0]&&parseInt(paciente.edad)<=this.frecuencia[f][1]){
        this.aux=0;
        for(var c:number=2; c<=6;c+=2){
          if(parseInt(this.cardiovascular.fcardiaca)>=this.frecuencia[f][c]&&parseInt(this.cardiovascular.fcardiaca)<=this.frecuencia[f][c+1]){
            this.valorfrecuencia=this.aux;
          }else if(parseInt(this.cardiovascular.fcardiaca)>=this.frecuencia[f][8]){
            this.valorfrecuencia=3;
          }
          this.aux++; 
        }
      }
    }
    //console.log(this.valorfrecuencia);
    switch(this.valorfrecuencia){
      case 0: return this.estadofrecuencia="NORMAL";
      case 1: return this.estadofrecuencia="LEVE";
      case 2: return this.estadofrecuencia="MODERADA";
      case 3: return this.estadofrecuencia="SEVERA";
      default: return this.estadofrecuencia="";
    }
  }

  Cap(){
    if(parseInt(this.cardiovascular.llenado)>5){
      this.estadocapilar="SEVERA";
      this.valorcapilar=3;
    } else if(parseInt(this.cardiovascular.llenado)==4||parseInt(this.cardiovascular.llenado)==5) {
      this.estadocapilar="MODERADA";
      this.valorcapilar=2;
    }else if(parseInt(this.cardiovascular.llenado)==3){
      this.estadocapilar="LEVE";
      this.valorcapilar=1;
    }else {
      this.estadocapilar="NORMAL";
      this.valorcapilar=0;
    }
  }
  
  async Siguiente(){
    this.cardiovascular.estado=("Frecuencia Cardiaca: "+this.cardiovascular.fcardiaca+" latidos/min ["+ this.estadofrecuencia+
                                  "],\n\r Llenado capilar: "+this.cardiovascular.llenado+" segundos ["+this.estadocapilar+"].");
    if(this.valorcapilar>=this.valorfrecuencia){
      this.cardiovascular.valor=this.valorcapilar;
    }else{
      this.cardiovascular.valor=this.valorfrecuencia;
    }
    
    sessionStorage.setItem("ParametrosCardio", JSON.stringify(this.cardiovascular));
    
    if(this.cardiovascular.valor==3){
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Alerta',
        subHeader:'Evaluación Cardiovascular',
        message: 'El paciente presenta un estado Cardiovascular SEVERO, por lo que se procederá con la evaluación Respiratoria.',
        buttons: ['OK']
     });
     await alert.present();
      this.router.navigate(['/respiratorio4']);
    }else{
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Alerta',
        subHeader:'Evaluación Cardiovascular',
        message: 'El paciente NO presenta un estado Cardiovascular SEVERO, puede continuar con la evaluación.',
        buttons: ['OK']
     });
     await alert.present();
      this.router.navigate(['/cardiovascular3']);
    }
    
  }
}
