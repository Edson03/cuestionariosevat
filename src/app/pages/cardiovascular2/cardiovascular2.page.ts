import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cardiovascular2',
  templateUrl: './cardiovascular2.page.html',
  styleUrls: ['./cardiovascular2.page.scss'],
})
export class Cardiovascular2Page implements OnInit {

  constructor() { }
  data =[
    {
      label: ' ... tiene pulsos periféricos disminuidos? ',
      selected: false,
      value: 2,
      name: 'El paciente está marmóreo'
      }
  ]
  ngOnInit() {
  }

}
