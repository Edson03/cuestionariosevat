import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EstadoResp0PageRoutingModule } from './estado-resp0-routing.module';

import { EstadoResp0Page } from './estado-resp0.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EstadoResp0PageRoutingModule
  ],
  declarations: [EstadoResp0Page]
})
export class EstadoResp0PageModule {}
