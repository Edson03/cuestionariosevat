import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EstadoResp0Page } from './estado-resp0.page';

const routes: Routes = [
  {
    path: '',
    component: EstadoResp0Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EstadoResp0PageRoutingModule {}
