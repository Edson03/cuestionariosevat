import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-estado-resp0',
  templateUrl: './estado-resp0.page.html',
  styleUrls: ['./estado-resp0.page.scss'],
})
export class EstadoResp0Page implements OnInit {

  constructor(public alertController: AlertController, private router: Router) { }
  item(x: number, y: string) {
    this.router.navigate(['/extra']);
  }
  async alerta() {
    const alert = await this.alertController.create({
      header: 'Alerta',
      subHeader: 'Usted no selecciono ninguna opcion',
      message: 'Seleccione una opcion por favor',
      buttons: ['OK']
    });

    await alert.present();
  }

  ngOnInit() {
  }

}
