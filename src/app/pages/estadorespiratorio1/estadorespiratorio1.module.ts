import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Estadorespiratorio1PageRoutingModule } from './estadorespiratorio1-routing.module';

import { Estadorespiratorio1Page } from './estadorespiratorio1.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Estadorespiratorio1PageRoutingModule,
    ComponentsModule
  ],
  declarations: [Estadorespiratorio1Page]
})
export class Estadorespiratorio1PageModule {}
