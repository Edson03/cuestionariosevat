import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Popinfo1Component } from '../../components/popinfo1/popinfo1.component';

@Component({
  selector: 'app-estadorespiratorio1',
  templateUrl: './estadorespiratorio1.page.html',
  styleUrls: ['./estadorespiratorio1.page.scss'],
})
export class Estadorespiratorio1Page implements OnInit {

  constructor( private popoverCtrl: PopoverController ) { }

  ngOnInit() {
  }

  async mostrarPop1(evento ) {

    const popover = await this.popoverCtrl.create({
      component: Popinfo1Component,
      event: evento,
      mode: 'ios',
    });

    await popover.present();
  }

}
