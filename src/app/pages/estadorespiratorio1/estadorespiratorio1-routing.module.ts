import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Estadorespiratorio1Page } from './estadorespiratorio1.page';

const routes: Routes = [
  {
    path: '',
    component: Estadorespiratorio1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Estadorespiratorio1PageRoutingModule {}
