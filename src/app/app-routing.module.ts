import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'inicio', pathMatch: 'full' },
  {
    path: 'cardiovascular1',
    loadChildren: () => import('./pages/cardiovascular1/cardiovascular1.module').then( m => m.Cardiovascular1PageModule)
  },
  {
    path: 'estadorespiratorio3',
    loadChildren: () => import('./pages/estadorespiratorio3/estadorespiratorio3.module').then( m => m.Estadorespiratorio3PageModule)
  },
  {
    path: 'estadorespiratorio2',
    loadChildren: () => import('./pages/estadorespiratorio2/estadorespiratorio2.module').then( m => m.Estadorespiratorio2PageModule)
  },
  {
    path: 'estadorespiratorio1',
    loadChildren: () => import('./pages/estadorespiratorio1/estadorespiratorio1.module').then( m => m.Estadorespiratorio1PageModule)
  },
  {
    path: 'cardiovascular2',
    loadChildren: () => import('./pages/cardiovascular2/cardiovascular2.module').then( m => m.Cardiovascular2PageModule)
  },
  {
    path: 'cardiovascular3',
    loadChildren: () => import('./pages/cardiovascular3/cardiovascular3.module').then( m => m.Cardiovascular3PageModule)
  },
  {
    path: 'cardiovascular',
    loadChildren: () => import('./pages/cardiovascular/cardiovascular.module').then( m => m.CardiovascularPageModule)
  },
  {
    path: 'cardiovascular0',
    loadChildren: () => import('./pages/cardiovascular0/cardiovascular0.module').then( m => m.Cardiovascular0PageModule)
  },
  {
    path: 'cardiovascular2',
    loadChildren: () => import('./pages/cardiovascular2/cardiovascular2.module').then( m => m.Cardiovascular2PageModule)
  },
  {
    path: 'respiratorio4',
    loadChildren: () => import('./pages/respiratorio4/respiratorio4.module').then( m => m.Respiratorio4PageModule)
  },
  {
    path: 'resultados',
    loadChildren: () => import('./pages/resultados/resultados.module').then( m => m.ResultadosPageModule)
  },
  //agregado por Abraham
  {
    path: 'neurologico1',
    loadChildren: () => import('./pages/neurologico1/neurologico1.module').then( m => m.Neurologico1PageModule)
  },
  {
    path: 'pagina2',
    loadChildren: () => import('./pages/Neurologico P2/pagina2.module').then( m => m.Pagina2PageModule)
  },
  {
    path: 'pagina3',
    loadChildren: () => import('./pages/Neurologico P3/pagina3.module').then( m => m.Pagina3PageModule)
  },
  {
    path: 'inicio',
    loadChildren: () => import('./pages/inicio/inicio.module').then( m => m.InicioPageModule)
  },
  {
    path: 'datos-pacientes',
    loadChildren: () => import('./pages/datos-pacientes/datos-pacientes.module').then( m => m.DatosPacientesPageModule)
  },
  {
    path: 'estado-neurologico',
    loadChildren: () => import('./pages/estado-neurologico/estado-neurologico.module').then( m => m.EstadoNeurologicoPageModule)
  },
  {
    path: 'estado-resp0',
    loadChildren: () => import('./pages/estado-resp0/estado-resp0.module').then( m => m.EstadoResp0PageModule)
  },
  //{
    //path: 'salida/:estado/:opcion_punteo/:punteo',
    //loadChildren: () => import('./pages/salida/salida.module').then( m => m.SalidaPageModule)
  //},
  //salida es la hoja de resultados y que valores se le van a dar son, estado,opcion punteo y punteo.
  {
    path: 'extra',
    loadChildren: () => import('./pages/extra/extra.module').then( m => m.ExtraPageModule)
  },//esta pagina extra es la segunda de la ultima hoja de la maqueta.   

  //final de agregado por Abraham

  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
